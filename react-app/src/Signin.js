import React, {useState} from "react";
import { useHistory } from "react-router-dom";

const Signin =()=>{

    const history = useHistory();

    const [email,setEmail]=useState("");
    const [password,setPassword]=useState("");

    function signIn(){
        let data={email,password}
        fetch("http://localhost:5000/signin",{
        method:'POST',
        headers:{
          'Accept':'application/json',
          'Content-Type':'application/json'
        },
        body:JSON.stringify(data) 
        })
        .then(response => response.json()
        
        .then((user_result)=>{

            if(user_result)
            {
                history.push('/',{data:user_result});
            }

            else
            {
                history.push('/signin')
            }

        })).catch(err=>{

            alert("Invalid Username and Password");
            history.push('/signin')
          })

    }

return(
  
        <div className="my-5">
        <h1 className="text-center">SignIn</h1>

        <div className="contact_div">
        <div className="row">
            <div className="col-md-6">
            <div class="md-3">
            <label for="Input2" class="form-label">Email</label>
            <input type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}  name="email" class="form-control" id="Input2"  placeholder="Email address" required autofocus />
            </div>

            <div class="md-3">
            <label for="Input3" class="form-label">Password</label>
            <input type="password" value={password}  onChange={(e)=>{setPassword(e.target.value)}} name="password" class="form-control" id="Input3"  placeholder="password" required autofocus />
            </div>

            <div class="col-12">  
            <button  onClick={signIn} class="btn btn-outline-primary" type="button">Submit</button>
            </div> 

            </div>
        </div>
        </ div>

        </ div>


);

}

export default Signin;