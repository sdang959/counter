import React, {useState} from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";

function Signup(){

    const history = useHistory();

    const [name,setName]=useState("");
    const [email,setEmail]=useState("");
    const [password,setPassword]=useState("");

    function signUp(){
        let data={name,email,password}
        fetch("http://localhost:5000/signup",{
        method:'POST',
        headers:{
          'Accept':'application/json',
          'Content-Type':'application/json'
        },
        body:JSON.stringify(data) 
        })
        .then(response => response.json()
        .then((user)=>{

          if(user)
            {
                alert("User Created Successfully in Database!");
                history.push('/signin')

            }

          else
          {
            console.log("error");
            history.push('/signup')    
          }

        })).catch(err=>{
          console.log(err);
          history.push('/signup')
        })
      
        
    }

return(
   <div className="my-5">
   <h1 className="text-center">SignUp</h1>

   <div className="contact_div">
    <div className="row">
        <div className="col-md-6">
          <div class="md-3">
          <label for="Input1" class="form-label">Name</label>
          <input type="text"  value={name} onChange={(e)=>{setName(e.target.value)}}  name="name" class="form-control" id="Input1"  placeholder="Enter your name" required autofocus />
          </div>

          <div class="md-3">
          <label for="Input2" class="form-label">Email</label>
          <input type="email" value={email} onChange={(e)=>{setEmail(e.target.value)}}  name="email" class="form-control" id="Input2"  placeholder="Email address" required autofocus />
          </div>

          <div class="md-3">
          <label for="Input3" class="form-label">Password</label>
          <input type="password" value={password}  onChange={(e)=>{setPassword(e.target.value)}} name="password" class="form-control" id="Input3"  placeholder="password" required autofocus />
          </div>
 
          <div class="col-12">
          <button  onClick={signUp} class="btn btn-outline-primary" type="button">Submit</button>
          </div> 

         </div>
    </div>
   </ div>

  </ div>

);

}

export default Signup;